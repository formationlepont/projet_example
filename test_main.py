''' 
content of test_sample.py
'''
def inc(x):
    '''
    function + 1
    '''
    return x + 1


def test_answer():
    '''
    test de inc
    '''
    assert inc(3) == 4
